# Pavillon Noir v2 feuille de personnage en français

## Usage de pnroll et fencingcard

pnroll est le module servant à effectuer des jets de dés et obtenir les résultats dans le chat.
fencingcard est le module servant à détecter des manoeuvres d'escrime disponible à un personnage escrimeur lors d'une attaque. Il est facultatif.

Les deux modules utilisent PowerCards.js, un script disponible pour les utilisateurs Pro de Roll20 dans la Script Library. De fait, cette feuille de personnage ne peut pas fonctionner sans un compte pro.

Il existe 5 commandes pnroll :

- pnskill
- pnattribute
- pnattack
- pnlocalisation
- pnluck

Et une commande du module fencingcard :

- pnfencingmode

### pnskill

Lance un jet de compétence

    !pnskill @{attributename}_@{attributelvl}|@{skillname}|@{skilllvl}|@{job}|@{bonusefficacité}|@{bonusfacilité}(optionnel:+@{malusdesanté})

ATTENTION @{attributename}\_@{attributelvl} sont séparés d'un underscore, à cause des contraintes d'usage des macros dans l'html
ATTENTION @{bonusfacilité}+@{malusdesanté} sont séparés d'un plus, pour représenter l'addition des 2 champs

- attributename
  un texte désignant le nom de l'attribut
- attributelvl
  un nombre désignant la valeur de l'attribut
- skillname
  un texte désignant le nom de la compétence
- skilllvl
  un nombre désignant la valeur de la compétence
- job
  un texte désignant si la compétence est de métier ou non. Si "job" est renseigné c'est une compétence de métier, autrement elle n'est pas métier
- bonusefficacité
  un nombre désignant la valeur du bonus d'efficacité
- bonusfacilité
  un nombre désignant la valeur du bonus de facilité
- malusdesanté
  un nombre désignant la valeur du malus de santé global imposé par une blessure. Rempli automatiquement sur les fiches de personnages, non nécessaire pour commande manuelle.

### pnattribute

Lance un jet d'attribut

    !pnattribute @{attributename}|@{attributelvl}

pour le coup trivial :

- attributename
  un texte désignant le nom de l'attribut
- attributelvl
  un nombre désignant la valeur de l'attribut

### pnattack

Lance un jet d'attaque

    !pnattack @{attributename}|@{attributelvl}|@{skillname}|@{skilllvl}|@{MD}|@{MDArme}|@{bonusefficacité}|@{bonusfacilité}(optionnel:+@{malusdesanté})|fencingType|fencingMove

ATTENTION @{bonusfacilité}+@{malusdesanté} sont séparés d'un plus, pour représenter l'addition des 2 champs
ATTENTION si fencingType et fencingMove ne sont pas utilisés, le dernier "|" peut être ignoré. Exemple : Force|7|Hache|3|2|1|0|0| et Force|7|Hache|3|2|1|0|0|| fonctionneront

- attributename
  un texte désignant le nom de l'attribut
- attributelvl
  un nombre désignant la valeur de l'attribut
- skillname
  un texte désignant le nom de la compétence
- skilllvl
  un nombre désignant la valeur de la compétence
- MD
  un nombre désignant la valeur du modificateur de dégats à utiliser (Force ou Adresse)
- MDArme
  un nombre désignant la valeur des dégats à ajouter propre à l'arme
- bonusefficacité
  un nombre désignant la valeur du bonus d'efficacité
- bonusfacilité
  un nombre désignant la valeur du bonus de facilité
- malusdesanté
  un nombre désignant la valeur du malus de santé global imposé par une blessure. Rempli automatiquement sur les fiches de personnages, non nécessaire pour commande manuelle
- fencingType
  un texte désignant les types d'escrime disponibles pour le jet. Ce texte peut être laissé vide. Les types d'escrimes reconnus sont les suivants : "épéeescrime", "armedagueescrime", "deuxarmesescrime", "sabreescrime", "baïonetteescrime", "hacheescrime", "pontescrime", "destrezaescrime".
  On peut lister plusieurs types d'escrimes en les séparant d'un "&" comme dans cet exemple : épéeescrime&armedagueescrime
- fencingMoves
  un texte désignant si les manoeuvres d'escrime disponibles pour le jet. Ce texte peut être laissé vide. Les mouvements d'escrimes reconnus sont nombreux et sont trouvable dans l'html "Pavillon Noir 2.html" (regarder les champs "manoeuvrenom").
  On peut lister plusieurs manoeuvres d'escrimes en les séparant d'un "&" comme dans cet exemple : Botte&Parade de Prime

### pnlocalisation

Lance un dé de localisation

    !pnlocalisation 

### pnluck

Lance un dé de chance

!pnluck @{percentage}|@{luckvalue}

  - percentage
  un nombre désignant le pourcentage de réussite sur une base 10 (eg, 2 correspond à 20%)
  - luckvalue
  un nombre désignant la valeur de chance

### pnfencingmode

Fixe le mode d'escrime à utiliser

!pnfencingmode @{modevalue}

  - modevalue
  un texte désignant le mode à utiliser (draw, mastery ou strategy)

### Automatisme des Cartes d'Escrimes (Fonctionnel pour le besoin actuel, à peaufiner)

Lorsque la checkbox de "Epée seule" est côchée dans Escrime, les cartes d'escrime de l'épée seule sont disponibles et recherchées dans le cadre d'un jet d'attaque. En fonction des opportunités disponibles et des cartes d'escrimes disponibles au joueur, différentes cartes d'escrimes seront présentées.

A terme, toute les cartes d'escrimes doivent être implémentées, et l'automatisme devra pouvoir s'adapter à l'arme d'escrime utilisée. Pour le moment c'est encore très expérimental.