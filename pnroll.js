//global variables for the script
var fencingCardsLoaded = false;
							
const tabAttributeColor = {
	"Adresse" : "#0a8d0d",     //green
	"Force": "#c81b1d",        //red
	"Résistance": "#a06a0a",   //brown 
	"Adaptabilité": "#2cbfc7", //cyan
	"Erudition": "#a49898",    //white
	"Charisme": "#952d3a",     //pink
	"Perception": "#2a65bf",   //blue
	"Expression": "#fc9303",   //gold
	"Pouvoir": "#ffd700",      //yellow
	"Métier": "#000000"        //black
};

const tabTarget = {
	1 : "Jambe gauche",
	2 : "Jambe droite",
	3 : "Torse",
	4 : "Bras gauche",
	5 : "Bras droit",
	6 : "Tête"
}

on("ready", function() {
    log("pnroll.js loaded !");
});

on('chat:message', function(msg) {
    if(msg.type != 'api' || msg.content.indexOf('!')) return;
    
    var arrayOfCommands = ["pnskill", "pnattribute", "pnattack", "pnlocalisation", "pnluck"];
	
	var tabMsgContent = msg.content.substring(1).split(" ");
	var command = tabMsgContent[0];
	var tabArgs = tabMsgContent;
	tabArgs.shift();
	log("Commande : " + command);
	
	if(arrayOfCommands.indexOf(command) == -1)
	{
	    if(command === "power")
	    {
            log("Commande 'power' lancée via PowerCards.js !");
	    }
		else if (command === "pnfencingmode")
		{
			//ignore
		}
	    else
	    {
            log("ERREUR : mauvaise commande !");
	    }
	}
	
	//example of command : !pnskill @{attributename}_@{attributelvl}|@{skillname}|@{skilllvl}|job|@{bonusefficacité}|@{bonusfacilité}(optionnel:+@{malusdesanté})
	switch(command){
		case "pnskill":
			if(tabArgs.length > 0)
			{
			    var arg = tabArgs.join(" ");
			    if(arg != "")
			    {
			        log(arg);
			        var tabAttributeskill = arg.split("|");
			        if(tabAttributeskill.length == 6)
			        {
						//parse attributename and its value from macro
						var tabAttributeNameValue = tabAttributeskill[0].split('_');
			            var attributeName = tabAttributeNameValue[0];
						var attributeLvl = parseInt(tabAttributeNameValue[1], 10);
						//other values are simpler
			            var skillName = tabAttributeskill[1];
			            var skillLvl = parseInt(tabAttributeskill[2], 10);
						var isJobSkill = ( tabAttributeskill[3] == 'job');
						var efficiency = parseInt(tabAttributeskill[4], 10);
						//detect if a health malus is attached
						var tabFacility = tabAttributeskill[5].split('+');
						var facility = 0;
						if (tabFacility.length > 1)
						{
							facility = parseInt(tabFacility[0], 10) + parseInt(tabFacility[1], 10);;
						}
						else
						{
							facility = parseInt(tabFacility[0], 10);
						}
						
						var number_of_rolls = skillLvl + efficiency;
						var threshold = attributeLvl + facility;
						var rolls = standardRoll(number_of_rolls, isJobSkill);

        				sendChat("System", rolls, function(fmsg) {
							var resultMessage = "";
							var successes = 0;
							var result = formatRollResult(fmsg[0].inlinerolls);
							var successes = successCount(threshold, fmsg[0].inlinerolls);
        				    
                            resultMessage += "!power";
                            resultMessage += " {{";
                            resultMessage += " --bgcolor|" + tabAttributeColor[attributeName];
                            resultMessage += " --name|Test de compétence";
                            resultMessage += " --leftsub|" + skillName;
                            resultMessage += " --rightsub|" + attributeName;
                            resultMessage += " --Joueur:|" + msg.who;
                            resultMessage += " --Niveau d'attribut:|[! " + attributeLvl + " !]";
                            resultMessage += show_hide_line("Facilité", facility);
                            resultMessage += " --Niveau de compétence:|[! " + skillLvl + " !]";
                            resultMessage += show_hide_line("Efficacité", efficiency);
                            resultMessage += " --Résultat:|[! " + result + " !]";
                            resultMessage += " --Succès:|[! " + successes + " !]";
                            if(successes == 0)
                            {
								for (let i = 0; i < fmsg[0].inlinerolls.length; i++ )
								{
									if (fmsg[0].inlinerolls[i].results.total >= 10)
									{
										resultMessage += " --Echec critique - Vous échouez lamentablement";
										break;
									}
								}
							}
							else if (successes >= skillLvl)
							{
								resultMessage += " --Gros Chatard coche ta case ! (si c'est pas déjà fait)";
							}
                            resultMessage += " }}";

        				    sendChat("System", resultMessage);
        				});
			        }
			        else
			        {
			            sendChat("System", "ERREUR : le ou les arguments sont invalides !");
			        }
			    }
			    else
			    {
			        sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			    }
			}
			else
			{
				sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			}
			break;
			
		//example of command : !pnattribute @{attributename}|@{attributelvl}|(optionnel:@{malusdesanté})
		case "pnattribute":
			if(tabArgs.length > 0)
			{
			    var arg = tabArgs.join(" ");
			    if(arg != "")
			    {
			        log(arg);
			        var tabAttributeskill = arg.split("|");
			        if(tabAttributeskill.length >= 2)
			        {
			            var attributeName = tabAttributeskill[0];
						var attributeLvl = parseInt(tabAttributeskill[1], 10);
						var healthMalus = 0;
						if (tabAttributeskill.length >= 3)
						{
							healthMalus = parseInt(tabAttributeskill[2], 10);
						}
						var threshold = attributeLvl + healthMalus;
						
						var rolls = "[[1d10]] [[1d10]]";

        				sendChat("System", rolls, function(fmsg) {
							var resultMessage = "";
							var result = formatRollResult(fmsg[0].inlinerolls);
							var successes = successCount(threshold, fmsg[0].inlinerolls);
        				    
                            resultMessage += "!power";
                            resultMessage += " {{";
                            resultMessage += " --bgcolor|" + tabAttributeColor[attributeName];
                            resultMessage += " --name|Test d'attribut";
                            resultMessage += " --leftsub|" + attributeName;
                            resultMessage += " --Joueur:|" + msg.who;
							resultMessage += " --Niveau d'attribut:|[! " + attributeLvl + " !]";
							if (healthMalus != 0)
							{
								resultMessage += " --Malus de Santé:|[! " + healthMalus + " !]";
							}
                            resultMessage += " --Résultat:|[! " + result + " !]";
                            resultMessage += " --Succès:|[! " + successes + " !]";
                            if(successes == 0)
                            {
								for (let i = 0; i < fmsg[0].inlinerolls.length; i++ )
								{
									if (fmsg[0].inlinerolls[i].results.total >= 10)
									{
										resultMessage += " --Echec critique - Vous échouez lamentablement";
										break;
									}
								}
							}
                            resultMessage += " }}";

        				    sendChat("System", resultMessage);
        				});
			        }
			        else
			        {
			            sendChat("System", "ERREUR : le ou les arguments sont invalides !");
			        }
			    }
			    else
			    {
			        sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			    }
			}
			else
			{
				sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			}
			break;
			
	//example of command : !pnattack @{attributename}|@{attributelvl}|@{skillname}|@{skilllvl}|@{MDFor/MDAdr}|@{MDArme}|@{bonusefficacité}|@{bonusfacilité}(optionnel:+@{malusdesanté})|fencing|fencingmoves
		case "pnattack":
			if(tabArgs.length > 0)
			{
			    var arg = tabArgs.join(" ");
			    if(arg != "")
			    {
			        log(arg);
			        var tabAttributeskill = arg.split("|");
			        if(tabAttributeskill.length == 9 || tabAttributeskill.length == 10)
			        {
			            var attributeName = tabAttributeskill[0];
						var attributeLvl = parseInt(tabAttributeskill[1], 10);
						//other values are simpler
			            var skillName = tabAttributeskill[2];
			            var skillLvl = parseInt(tabAttributeskill[3], 10);
			            var MDAttribute = parseInt(tabAttributeskill[4], 10);
			            var MDWeapon = parseInt(tabAttributeskill[5], 10);
			            var MDTotal = MDAttribute + MDWeapon;
						var efficiency = parseInt(tabAttributeskill[6], 10);
						//detect if a health malus is attached
						var tabFacility = tabAttributeskill[7].split('+');
						var facility = 0;
						if (tabFacility.length > 1)
						{
							facility = parseInt(tabFacility[0], 10) + parseInt(tabFacility[1], 10);
						}
						else
						{
							facility = parseInt(tabFacility[0], 10);
						}
						var isFencing = ( tabAttributeskill[8] != "");
						var fencingStringList = "";
						if (isFencing) { fencingStringList = tabAttributeskill[8]; }
						var fencingMovesStringList = "";
						if (tabAttributeskill.length == 10)
						{
							if ( tabAttributeskill[9] != "")
							{
								fencingMovesStringList = tabAttributeskill[9];
							}
						}
						
						var target_roll = "[[1d6]] ";
						var number_of_rolls = skillLvl + efficiency;
						var threshold = attributeLvl + facility;
						var rolls = standardRoll(number_of_rolls, false);

        				sendChat("System", target_roll + rolls, function(fmsg) {
							var resultMessage = "";
							var target = fmsg[0].inlinerolls[0].results.total;
							fmsg[0].inlinerolls.shift(); //target roll out !
							var result = formatRollResult(fmsg[0].inlinerolls);
							var successes = successCount(threshold, fmsg[0].inlinerolls);
        				    
                            resultMessage += "!power";
                            resultMessage += " {{";
                            resultMessage += " --bgcolor|" + tabAttributeColor[attributeName];
                            resultMessage += " --name|Test d'Attaque";
                            resultMessage += " --leftsub|" + skillName;
                            resultMessage += " --rightsub|" + attributeName;
                            resultMessage += " --Joueur:|" + msg.who;
                            resultMessage += " --Niveau d'attribut:|[! " + attributeLvl + " !]";
                            resultMessage += show_hide_line("Facilité", facility);
                            resultMessage += " --Niveau de compétence:|[! " + skillLvl + " !]";
                            resultMessage += show_hide_line("Efficacité", efficiency);
                            resultMessage += " --Résultat:|[! " + result + " !]";
                            resultMessage += " --Succès:|[! " + successes + " !]";
                            if(successes == 0)
                            {
								for (let i = 0; i < fmsg[0].inlinerolls.length; i++ )
								{
									if (fmsg[0].inlinerolls[i].results.total >= 10)
									{
										resultMessage += " --Echec critique - Vous échouez lamentablement";
										break;
									}
								}
							}
							else if (successes >= skillLvl)
							{
								resultMessage += " --Gros Chatard coche ta case ! (si c'est pas déjà fait)";
							}
                            resultMessage += " --Modificateur d'attribut:|[! " + MDAttribute + " !]";
                            resultMessage += " --Modificateur d'arme:|[! " + MDWeapon + " !]";
                            resultMessage += " --Modificateur total de dégâts:|[! " + MDTotal + " !]";
                            resultMessage += " --Localisation:|[! " + tabTarget[target] + " !]";

							if(isFencing && fencingCardsLoaded)
							{
								var tabFencingType = parseFencingType(fencingStringList);
								var tabFencingMove = [];
								if (fencingMovesStringList != "")
								{
									tabFencingMove = parseFencingMoves(fencingMovesStringList);
								}

								//check the weapon can use a fencing move
								if (isFencingWeapon(skillName,tabFencingType))
								{

									var opportunities_array = opportunities(tabFencingType, fmsg[0].inlinerolls);
									var opportunities_string = "";
									for (let i = 0; i < opportunities_array.length; i++ )
									{
										opportunities_string += opportunities_array[i] + " ";
									}
									
									resultMessage += " --Opportunités:|[! " + opportunities_string + " !]";
									resultMessage += " }}";
									sendChat("System", resultMessage);
									
									if (opportunities_string != "")
									{
										var drawnFencingCards = findFencingCards(opportunities_array, tabFencingMove);
										for (let i = 0; i < drawnFencingCards.length; i++ )
										{
											resultMessage = drawnFencingCards[i].buildMessage(msg.who);
											sendChat("System", resultMessage);
										}
									}
								}
								else{
									resultMessage += " }}";
									sendChat("System", resultMessage);
								}
							}
							else{
								resultMessage += " }}";
								sendChat("System", resultMessage);
							}

							
        				});
			        }
			        else
			        {
			            sendChat("System", "ERREUR : le ou les arguments sont invalides !");
			        }
			    }
			    else
			    {
			        sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			    }
			}
			else
			{
				sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			}
			break;

	//example of command : !pnlocalisation
		case ("pnlocalisation") :
			var target_roll = "[[1d6]]";
			sendChat("System", target_roll, function(fmsg) {
				var resultMessage = "";
				var target = fmsg[0].inlinerolls[0].results.total;
				resultMessage += "!power";
				resultMessage += " {{";
				resultMessage += " --bgcolor|" + "#000000";
				resultMessage += " --name|Test de Localisation";
				resultMessage += " --Localisation:|[! " + tabTarget[target] + " !]";
				resultMessage += " }}";
				sendChat("System", resultMessage);
        	});
			break;

	//example of command : !pnluck @{percentage}|@{luckvalue}
		case ("pnluck") :
			if(tabArgs.length > 0)
			{
			    var arg = tabArgs.join(" ");
			    if(arg != "")
			    {
			        log(arg);
			        var tabAttributeskill = arg.split("|");
			        if(tabAttributeskill.length == 2)
			        {
			            var percentage = parseInt(tabAttributeskill[0]);
						var luckValue = parseInt(tabAttributeskill[1], 10);
						var threshold = percentage + luckValue;
						
						var roll = "[[1d10]]";

        				sendChat("System", roll, function(fmsg) {
							var resultMessage = "";
							var success = 0;
							var result = "";

							result += "(" + fmsg[0].inlinerolls[0].results.total + ")";
							if (fmsg[0].inlinerolls[0].results.total <= threshold) {
								success = 1;
							}
							else {
								//no success !
							}
        				    
                            resultMessage += "!power";
                            resultMessage += " {{";
                            resultMessage += " --name|Test de Chance";
                            resultMessage += " --Joueur:|" + msg.who;
                            resultMessage += " --Pourcentage de Chance:|[! " + percentage + " !]";
                            resultMessage += " --Valeur de Chance:|[! " + luckValue + " !]";
							resultMessage += " --Résultat:|[! " + result + " !]";
							if (success)
							{
								resultMessage += " --bgcolor|" + "#66CC00";
								resultMessage += " --Succès ! ";
							}
							else
							{
								resultMessage += " --bgcolor|" + "#FF0000";
								resultMessage += " --Échec ! ";
							}
                            resultMessage += " }}";

        				    sendChat("System", resultMessage);
        				});
			        }
			        else
			        {
			            sendChat("System", "ERREUR : le ou les arguments sont invalides !");
			        }
			    }
			    else
			    {
			        sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			    }
			}
			else
			{
				sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le lancer !");
			}
			break;
	}
	
});

//standard roll function
function standardRoll(number_of_rolls, isJobSkill) {
	rolls = "";
	// number of rolls depends on skill Lvl
	if (number_of_rolls == 0) {
		if (isJobSkill) {
			rolls += "[[1d20]]"
		}
		else {
			rolls += "[[1d12]]"
		}
	}
	else if (number_of_rolls < 0) {
		rolls += "[[1d100]]"
	}
	else {
		for (let i = 0; i < number_of_rolls; i++) {
			rolls += "[[1d10]] "
		}
	}
	return rolls;			
}

function formatRollResult(inlinerolls)
{
	var result = "";
	//TODO : seems like bad implementation
	for (let i =0; i < inlinerolls.length; i++ )
	{
		result += "(" + inlinerolls[i].results.total + ")  ";
	}
	return result;
}

//standard success count function
function successCount(threshold, inlinerolls) {
	var successes = 0;
	//threshold cannot be superior than 9
	if (threshold <= 0)
	{
		//no success !
		result = "échec automatique !"
	}
	else 
	{
		if (threshold > 9)
		{
			for (let t = threshold; t > 9;t--)
			{
				successes +=1;
			}
			threshold = 9;
		}
		for (let i =0; i < inlinerolls.length; i++ )
		{
			if (inlinerolls[i].results.total == 1 && threshold != 1)
			{
				successes += 2;
			}
			else if (inlinerolls[i].results.total <= threshold) {
				successes += 1;
			}
			else {
				//no success !
			}
		}
	}
	return successes;
}

function show_hide_line(line, value)
{
	result = "";
	if (value != 0)
	{
		result += " --" + line + ":|[! " + value + " !]";
	}
	return result;
}