//global variable for the script
var fencingCards = new Map();
// base mode of fencing is mastery
var fencingMode = "mastery";

on("ready", function() {
    init();
	fencingCardsLoaded = true;
	log("fencingcard.js loaded !");
});

on('chat:message', function(msg) {
    if(msg.type != 'api' || msg.content.indexOf('!')) return;
    
    var arrayOfCommands = ["pnfencingmode"];
	
	var tabMsgContent = msg.content.substring(1).split(" ");
	var command = tabMsgContent[0];
	var tabArgs = tabMsgContent;
	tabArgs.shift();
	log("Commande : " + command);
	
	switch(command)
	{
		//example of command : !pnfencingmode mode
		case ("pnfencingmode") :
			if(tabArgs.length > 0)
			{
				var arg = tabArgs.join(" ");
				if(arg != "")
				{
			        log(arg);
					if (fencingCardsLoaded) {
						setFencingMode(arg);
					}
				}
			    else
			    {
			        sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le mode !");
			    }
			}
			else
			{
				sendChat("System", "ERREUR : vous n'avez pas fourni d'arguments pour le mode !");
			}
			break;
	}
	
});

function isFencingWeapon(weaponSkillName, tabFencingType)
{
	var result = false;
	log("isFencingWeapon");
	tabFencingType.forEach(fencingType => {
		if (fencingType.name == "épée seule")
		{
			result |= (weaponSkillName == "Rapière");
		}
		else if (fencingType.name == "arme et dague")
		{
			result |= (weaponSkillName == "Rapière") || (weaponSkillName == "Sabre");
		}
		else if (fencingType.name == "deux armes")
		{
			result |= (weaponSkillName == "Rapière") || (weaponSkillName == "Sabre");
		}
		else if (fencingType.name == "sabre")
		{
			result |= (weaponSkillName == "Sabre");
		}
		else if (fencingType.name == "baïonettee")
		{
			result |= (weaponSkillName == "Lance");
		}
		else if (fencingType.name == "hache")
		{
			result |= (weaponSkillName == "Hache");
		}
		else if (fencingType.name == "combat de pont")
		{
			//special case to investigate
			result |= (weaponSkillName == "Hache") || (weaponSkillName == "Rapière") || (weaponSkillName == "Sabre");
		}
		else 
		{
			log("fencing type not found");
		}
	});

	return result;
}

function opportunities(tabFencingType, inlinerolls)
{
	var result = [];
	var possible_opportunities = new Set([]);
	tabFencingType.forEach(fencingType => {
		fencingType.opportunities.forEach(opportunity => possible_opportunities.add(opportunity));
	});
	inlinerolls.forEach(roll => {  
		let rollResult = 0;
		if (roll.results.total != 10)
		{
			rollResult = roll.results.total;
		}
		if (possible_opportunities.has(rollResult))
		{
			result.push(rollResult);
		}
	});

	return result;
}

function setFencingMode(mode)
{
	fencingMode = mode;
	log("new fencingMode is " + fencingMode);
}

function findFencingCards(opportunities, args)
{
	let drawnFencingCards = [];
	log("opportunities : " + opportunities);
	const effectiveFencingCards = mode_adapter(args);
	//const effectiveFencingCards = fencingCards;
	/*log("effectiveFencingCards : ");
    effectiveFencingCards.forEach(function(value, key) {
      log(`${key} = ${value}`);
    });*/
	const keys = Array.from(effectiveFencingCards.keys());
	for (let i = 0; i < keys.length; i++ )
	{
		let result = fencingCards.get(keys[i]).isUsable(opportunities);
	    if (result)
	    {
	       drawnFencingCards.push(fencingCards.get(keys[i]));
	    }
	}

	return drawnFencingCards;
}

function createFencingCard(name, opportunity, flavor_text, efficiency, facility, successes, inappropriate, not_suitable) {
	var fencingCard = {};
	fencingCard.name = name;
	fencingCard.opportunity = opportunity;
	fencingCard.flavor_text = flavor_text;
	fencingCard.efficiency = efficiency;
	fencingCard.facility = facility;
	fencingCard.successes = successes;
	fencingCard.inappropriate = inappropriate;
	fencingCard.not_suitable = not_suitable;
	fencingCard.buildMessage = function (playername) {
		var resultMessage = "";
		
		resultMessage += "!power";
		resultMessage += " {{";
		resultMessage += " --bgcolor|" + "#000000";
		resultMessage += " --name|Carte d'Escrime";
		resultMessage += " --Joueur:|" + playername;
		resultMessage += " --Manoeuvre:| " + this.name;
		resultMessage += " --Opportunité:|[! " + this.opportunity+ " !]";
		resultMessage += " --Effet:|" + this.flavor_text;
		resultMessage += " --Efficacité:|[! " + this.efficiency + " !]";
		resultMessage += " --Facilité:|[! " + this.facility + " !]";
		resultMessage += " --Succès:|[! " + this.successes + " !]";
		if (this.not_suitable)
		{
			resultMessage += " --Peu Adapté:|[! " + this.not_suitable + " !]";
		}
		if (inappropriate)
		{
			resultMessage += " --Inadapté:|[! " + this.inappropriate + " !]";
		}
		resultMessage += " }}";

		return resultMessage;
	}

	fencingCard.isUsable = function (opportunities) {
		let rolls = this.opportunity.split(" ");
		let opportunities_copy = [...opportunities];
		let result = true;
		//log ("check " + opportunities_copy + " for rolls " + rolls);
	    for (let i = 0; i < rolls.length; i++ ) {
	        //optimisation : if we still need to find a roll and the array is empty, then there no need to continue
	        if (opportunities_copy.length == 0) {
	            result = false;
	            break;
	        }
	        const isRoll = (element) => element == parseInt(rolls[i]);
	        let index = opportunities_copy.findIndex(isRoll);
	        if (index != -1) {
	            opportunities_copy.splice(index, 1);
	            result = result && true;
	        }
	        else {
	            result = false;
	        }
	    }
	    //log ("result = " + result);
		return result;
	}

	fencingCard.toString = function () 
	{
      var ret = 'name : ' + this.name + ', opportunity : ' + this.opportunity + ', flavor_text : ' + this.flavor_text 
      + ', efficiency : ' + this.efficiency + ', facility : ' + this.facility + ', successes : ' + this.successes
      + ', inappropriate : ' + this.inappropriate + ', not_suitable : ' + this.not_suitable;
      return ret;
    }
	return fencingCard;
}

function createFencingType(name, opportunities) 
{
	var fencingType = {};
	fencingType.name = name;
	fencingType.opportunities = opportunities;
	log("createFencingType : " + fencingType.name + " " + fencingType.opportunities);
	fencingType.extractOpportunities = function (inlinerolls)
	{
		const extracted = inlinerolls.reduce((acc, roll) => {
			if(fencingType.opportunities.find(roll.result.total))
			{
				acc.push(roll.results.total);
			}
			return acc;
		}, {});
		return extracted
	}
	return fencingType;
}

function parseFencingType(fencingTypeString)
{
	var tabFencingType = [];
	const fencingTypes = fencingTypeString.split("&");
	fencingTypes.forEach(fencingType =>
	{
		if (fencingType == "épéeescrime")
		{
			var obj = createFencingType("épée seule", [0,1,2,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "armedagueescrime")
		{
			var obj = createFencingType("arme et dague", [0,1,2,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "deuxarmesescrime")
		{
			var obj = createFencingType("deux armes", [0,1,2,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "sabreescrime")
		{
			var obj = createFencingType("sabre", [0,1,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "baïonetteescrime")
		{
			var obj = createFencingType("baïonette", [0,1,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "hacheescrime")
		{
			var obj = createFencingType("hache", [0,1,5]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "pontescrime")
		{
			var obj = createFencingType("combat de pont", [3,6,9]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "destrezaescrime")
		{
			var obj = createFencingType("destreza", [1,2,3,4,5,6,7,8,9,0]);
			tabFencingType.push(obj);
		}
		else if (fencingType == "")
		{
			//ignore empty string
		}
		else
		{
			log("unknown fencing type : " + fencingType);
		}
	});
	return tabFencingType;
}

function parseFencingMoves(fencingMovesString)
{
	var tabFencingMoves = [];
	const fencingMoves = fencingMovesString.split("&");
	fencingMoves.forEach(fencingMove =>
	{
		tabFencingMoves.push(fencingMove);
	});
	return tabFencingMoves;
}


function init()
{	
	//escrime à l'épée seule
	fencingCards.set("Coup Circulaire", createFencingCard("Coup Circulaire", "1", "bonus de dégâts +2", 0, 0, 2, "", ""));
	fencingCards.set("Botte", createFencingCard("Botte", "1", "attaque assez efficace", 1, 0, 0, "", ""));
	fencingCards.set("Contrôle de la main libre", createFencingCard("Contrôle de la main libre", "2", "attaque ennemie assez difficile (-1)", 0, 0, 0, "sabre", "baïonette, hache"));
	fencingCards.set("Parade de la main libre", createFencingCard("Parade de la main libre", "2", "armure de 2, attaque adverse cible le bras gauche", 0, 0, 0, "hache, sabre", ""));
	fencingCards.set("Coupé sur pointe", createFencingCard("Coupé sur pointe", "5", "choix de localisation de l'attaque sauf jambes", 0, 0, 0, "baïonette", ""));
	fencingCards.set("Dégagement", createFencingCard("Dégagement", "5", "choix de localisation de l'attaque sauf tête", 0, 0, 0, "", ""));
	fencingCards.set("Parade pointe volante", createFencingCard("Parade pointe volante", "0", "armure de 2", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Quarte", createFencingCard("Parade de Quarte", "0", "armure de 1, torse invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Tierce", createFencingCard("Parade de Tierce", "0", "armure de 1, bras droit invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Seconde", createFencingCard("Parade de Seconde", "0", "armure de 1, jambe droite invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Septime", createFencingCard("Parade de Septime", "0", "armure de 1, jambe gauche invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Prime", createFencingCard("Parade de Prime", "0", "armure de 1, bras gauche invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Parade de Quinte", createFencingCard("Parade de Quinte", "0", "armure de 1, tête invulnérable", 0, 0, 0, "", ""));
	fencingCards.set("Battement", createFencingCard("Battement", "1 1", "bonus de 2 succès", 0, 0, 2, "", "hache"));
	fencingCards.set("Froissement", createFencingCard("Froissement", "1 1", "attaque très facile +3, choix de localisation de l'attaque adverse", 0, 3, 0, "baïonette, sabre", "hache"));
	fencingCards.set("Invite", createFencingCard("Invite", "1 5", "attaque efficace +2D, choix de localisation de l'attaque adverse", 2, 0, 0, "", ""));
	fencingCards.set("Rompre la mesure", createFencingCard("Rompre la mesure", "1 0", "annule les résultats identiques adverse. Peut annuler la manoeuvre escrime adverse en cas de coup fourré", 0, 0, 0, "baïonette", ""));
    fencingCards.set("Serrer la mesure", createFencingCard("Serrer la mesure", "1 0", "attaque ennemie assez difficile (-1). Peut conserver les effets de la manouevre précédente pour ce tour ci et le suivant", 0, 0, 0, "baïonette", ""));
	fencingCards.set("Passe", createFencingCard("Passe", "5 5", "Annule la réussite adverse", 0, 0, 0, "", ""));
	fencingCards.set("Coup de Flanconade", createFencingCard("Coup de Flanconade", "5 5", "attaque devient facile (+2) et attaque adverse difficile (-2)", 0, 2, 0, "", ""));
	fencingCards.set("Contre de Quarte", createFencingCard("Contre de Quarte", "5 0", "armure de 2, tête, jambe gauche et bras droit invulnérables", 0, 0, 0, "sabre", "hache"));
	fencingCards.set("Contre de Tierce", createFencingCard("Contre de Tierce", "5 0", "armure de 2, torse, bras gauche et jambe droit invulnérables", 0, 0, 0, "sabre", "hache"));
	fencingCards.set("Parade du cercle", createFencingCard("Parade du cercle", "0 0", "invulnérable à l'attaque", 0, 0, 0, "", "hache, sabre", ""));
	fencingCards.set("Ecole d'escrime", createFencingCard("Ecole d'escrime, Cavé", "0 0 0", "réussite annulée, mais ennemi inflige ses dégâts à lui même", 0, 0, 0, "", "hache, sabre", ""));
	
    /*fencingCards.forEach(function(value, key) {
      log(`${key} = ${value}`);
    });*/
	log("fencingCards size = " + fencingCards.size);
}

//TODO : use a Strategy pattern with classes if possible
function mode_adapter(args) 
{
	var result;
	if (fencingMode == "draw") {
		result = draw_mode(args);
	}
	else if (fencingMode == "mastery") {
		result = mastery_mode(args);
	}
	else if (fencingMode == "strategy") {
		result = strategy_mode(args);
	}
	else
	{
		log("Unrecognized fencing mode : " + fencingMode);
		result = new Map();
	}
	return result;
}

function draw_mode(numberOfCardsToDraw)
{
    var effectiveFencingCards = new Map();
    let keys = Array.from(fencingCards.keys());
    for (let i = 0; i < numberOfCardsToDraw; i++) {
        var randIndex = Math.floor( Math.random() * keys.length );
        key = keys.splice( randIndex, 1 )[0];
        effectiveFencingCards.set(key, fencingCards.get(key));
    }
    var resultMessage = "";
		
	resultMessage += "!power";
	resultMessage += " {{";
	resultMessage += " --bgcolor|" + "#000000";
	resultMessage += " --name|Cartes d'Escrime Piochées";
	let i = 0;
	effectiveFencingCards.forEach(function(value, key) {
	    i++;
        resultMessage += " --Manoeuvre " + i + ":| " + key;
    });
	resultMessage += " }}";
    sendChat("System", resultMessage);
    
    return effectiveFencingCards;
}

function mastery_mode(keys) 
{
    var effectiveFencingCards = new Map();
    for (let i = 0; i < keys.length; i++)
    {
        effectiveFencingCards.set(keys[i], fencingCards.get(keys[i]));
    }
    return effectiveFencingCards;
}

function strategy_mode(keys) 
{
    var effectiveFencingCards = new Map();
    for (let i = 0; i < keys.length; i++)
    {
        effectiveFencingCards.set(keys[i], fencingCards.get(keys[i]));
    }
    return effectiveFencingCards;
}